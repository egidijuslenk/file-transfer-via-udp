#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void initConn(int& sockfd,  sockaddr_in &sender_addr) {
    // Send a connection request to the sender
    const char connection_request = 'C'; // Send a single byte as a connection request
    sendto(sockfd, &connection_request, sizeof(connection_request), 0, (struct sockaddr*)&sender_addr, sizeof(sender_addr));
};

void receiveFile(int& sockfd, std::ofstream& output_file, std::string& file_name){
    // Receive and write data to the output file
    const size_t buffer_size = 1024;
    char buffer[buffer_size];
    const char end_signal = 'E';

    while (true) {
        int bytes = recvfrom(sockfd, buffer, buffer_size, 0, nullptr, nullptr);
        std::cout << "bytes count: " << bytes << std::endl;
        if (bytes <= 0) {
            if (bytes < 0) {
                perror("recvfrom");
            };
            break; // End of file or error
        };

        // Check if received data matches the end signal
        if (bytes == 1 && buffer[0] == end_signal)
        {
            std::cout << std::endl << "Received end signal. File transmission complete." << std::endl;
            break;
        };
        output_file.write(buffer, bytes);
    };
    std::cout << "File saved as: " << file_name << std::endl;
    std::memset(buffer, 0, buffer_size);
};

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: " << argv[1] << " <sender_ip> <sender_port>" << std::endl;
        return 1;
    };

    const char* sender_ip_str = argv[1];
    const int sender_port = std::atoi(argv[2]);

    // Create a UDP socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket");
        return 1;
    };

    // Define the sender's address
    struct sockaddr_in sender_addr;
    memset(&sender_addr, 0, sizeof(sender_addr));
    sender_addr.sin_family = AF_INET;
    sender_addr.sin_port = htons(sender_port);

    // Convert the sender's IP address from string to binary
    if (inet_pton(AF_INET, sender_ip_str, &(sender_addr.sin_addr)) <= 0) {
        perror("inet_pton");
        close(sockfd);
        return 1;
    };

    initConn(sockfd, sender_addr);

    std::cout << "Waiting for the sender to return with file name" << std::endl;
    std::string file_name;
    // Wait for a file name from the receiver
    int bytes_received;
    char FileNameBuffer[1024];
    bytes_received = recvfrom(sockfd, FileNameBuffer, sizeof(FileNameBuffer), 0, nullptr, nullptr);

    if (bytes_received < 0) {
        perror("recvfrom");
        close(sockfd);
        exit(1);
    };

    file_name = std::string(FileNameBuffer);
    std::cout << "Received file name: " << file_name << "\nWaiting to receive data from the sender..." << std::endl;

    // Open the output file for writing
    std::ofstream output_file(file_name, std::ios::binary);
    if (!output_file) {
        std::cerr << "Error opening output file." << std::endl;
        close(sockfd);
        return 1;
    };

    receiveFile(sockfd, output_file, file_name);

    // Close the output file and socket
    output_file.close();
    close(sockfd);

    return 0;
}
