#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include <unistd.h>

sockaddr_in cathConnection( int& sockfd) {
std::cout << "Waiting for the receiver to initiate communication..." << std::endl;
struct sockaddr_in receiver_addr;
socklen_t receiver_addr_len = sizeof(receiver_addr);
int bytes_received;
char buffer[1]; // Receive a single byte as a connection request

bytes_received = recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr*)&receiver_addr, &receiver_addr_len);

if (bytes_received < 0) {
    perror("recvfrom");
    close(sockfd);
    exit(1);
}

std::cout << "Receiver initiated communication. Sending data..." << std::endl;
return receiver_addr;
};

double bytes_sent = 0.0;
void printStatus(int file_size, int bytes_of_file_sent) {
    double percentage = (static_cast<double>(bytes_of_file_sent) / file_size) * 100.0;
    
    if (percentage >= bytes_sent + 5) {
        std::cout << std::fixed << std::setprecision(0) << percentage << "%..." << std::endl;
        bytes_sent = percentage;
    }
}

void validateArgs(int argc, char* argv[], int *listen_port) {
    if (argc == 3) {
        *listen_port = std::atoi(argv[2]);
        std::cout << "Using " << argv[2] << " port" << std::endl;
    }
    else if (argc == 2){
        std::cout << "Using default " << *listen_port << " port" << std::endl;
    }
    else {
        std::cerr << "Usage: ./sender <name of file with extension> <port (default: " << *listen_port << ")>" << std::endl;
        exit(1);
    };
};

void sendFile(std::ifstream& file, int& sockfd, const std::string& file_name, sockaddr_in &receiver_addr, socklen_t &receiver_addr_len)  {

    // Move pointer to end of file, get its size Move pointer to begin of file
    file.seekg(0, std::ios::end);
    int file_size = static_cast<int>(file.tellg());
    std::cout << "File size: "<< file_size<< std::endl;
    std::cout << "File name: "<< file_name<< std::endl;
    file.seekg(0, std::ios::beg);

    // Send file name to receiver
    sendto(sockfd, file_name.c_str(), file_name.size(), 0, (struct sockaddr*)&receiver_addr, receiver_addr_len);

    // Read and send the file in chunks
    const size_t buffer_size = 1024;
    char file_buffer[buffer_size];
    int bytes_of_file_sent = 0;
    while (!file.eof()) {
        file.read(file_buffer, buffer_size);
        int bytes_read = file.gcount();
        if (bytes_read > 0) {
            // Send the chunk to the receiver
            sendto(sockfd, file_buffer, bytes_read, 0, (struct sockaddr*)&receiver_addr, receiver_addr_len);
            bytes_of_file_sent += bytes_read;
            std::cout << "bytes count: " << bytes_read << std::endl;
            // printStatus(file_size, bytes_of_file_sent);
        };
    };
    
    // Send the end signal to indicate the end of transmission
    char end_signal = 'E';
    sendto(sockfd, &end_signal, sizeof(end_signal), 0, (struct sockaddr*)&receiver_addr, receiver_addr_len);

    std::cout << "File transfer finished." << std::endl;
};

int main(int argc, char* argv[]) {
    int listen_port = 12350; // default port

    // Validate args
    validateArgs(argc, argv, &listen_port);

    const std::string file_name = argv[1];
    // Create a UDP socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket");
        return 1;
    }

    // Define the server address to listen on
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(listen_port);

    // Bind the socket to the server address
    if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
        perror("bind");
        close(sockfd);
        return 1;
    }

    // Wait for a connection request from the receiver
    sockaddr_in receiver_addr = cathConnection(sockfd);
    socklen_t receiver_addr_len = sizeof(receiver_addr);

    // Open the file for reading
    std::ifstream file(file_name, std::ios::binary);
    if (!file) {
        std::cerr << "Error opening file." << std::endl;
        close(sockfd);
        return 1;
    };
    sendFile(file, sockfd, file_name,  receiver_addr,  receiver_addr_len);

    // Close the file and socket
    file.close();
    close(sockfd);
    return 0;
}